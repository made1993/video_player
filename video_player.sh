#!/bin/bash

##############################################################################################################
##
##
##
##
##
##############################################################################################################

####################
##	VARIABLES
####################

##OLD
#DIR="/home/pi/videos/"
#DIR="/home/pi/Malcom/episodes/"
DIR="/home/pi/remote/series/los_simpsons/"

##NEW
LOCAL_DIR="/home/pi/videos/"
REMOTE_DIR="/home/pi/remote/series/" 

#REMOTE_HOST
#MEDIA_HOST="nfs.casa.local"
MEDIA_HOST="192.168.1.36"
MEDIA_DIR_REMOTE="./Videos/"
MEDIA_DIR_LOCAL="/home/pi/remote/"
#MEDIA_USER="media"
MEDIA_USER="media"

MOR_TIME="0600"
LNC_TIME="1100"
NGT_TIME="2000"

#MOR_SERIES="batman spiderman"
MOR_SERIES="los_simpsons futurama malcom south_park"
LNC_SERIES="los_simpsons futurama malcom south_park"
#LNC_SERIES="malcom futurama"
#NGT_SERIES="los_simpsons futurama"
NGT_SERIES="malcom south_park"

####################
##	FUNCTIONS
####################

################################################################################
## 	This function checks if the remote media server is alive.
##	If it is, then it tries to connect to mount the remote media.
################################################################################
mount_remote() {
	echo "Cheking $MEDIA_HOST alive"
	ping -c 1 $MEDIA_HOST &>/dev/null
	if [ "$?" = 0 ] 
	then
		echo "$MEDIA_HOST alive, mounting"
		sshfs $MEDIA_USER@$MEDIA_HOST:$MEDIA_DIR_REMOTE $MEDIA_DIR_LOCAL
	else
		echo "$MEDIA_HOST not alive"
	fi

}
################################################################################
## 	This function unmounts the remote local dir.
################################################################################
umount_remote() {
	if [ -d $REMOTE_DIR ]
	then
		umount -f  $MEDIA_DIR_LOCAL
	fi

}
################################################################################
##	This functions kills omxplayer processes if REMOTE_DIR is not present and 
##	the reproducer is supposed to use this directory.
##	Also this function should be executed as a background process.
################################################################################
remote_dir_whatchdog () {
	while true
	do
		if [ ! -d $REMOTE_DIR ] && [ $REMOTE_PLAY -eq 1 ]
		then
			killall omxplayer; killall omxplayer.bin
			export REMOTE_PLAY=0
		fi 
	done
	
}
################################################################################
##	This functions checks if the remote media server is alive:
##	It it is but the remote dir is not locally mounted then mounts it.
##	In other case checks the conditions after X seconds
################################################################################
remote_media_watchdog () {
	while true
	do
		ping -c 1 $MEDIA_HOST &>/dev/null
		if [ $? -eq 0 ] && [ $REMOTE_PLAY -eq 0 ] && [ ! -d $REMOTE_DIR ]
		then
			mount_remote
		fi
		sleep 10
	done


}
################################################################################
##	This function checks if the programation has changed. 
##	If it has changed returns true and false in the other case
################################################################################
programation_change () {
	
	if  test $CURR_TIME -ge $MOR_TIME  &&  test $CURR_TIME -le $LNC_TIME ; then
		PRGM="MOR"
	elif  test $CURR_TIME -ge $LNC_TIME && test $CURR_TIME -le $NGT_TIME ; then 
		PRGM="LNC"
	elif  test $CURR_TIME -ge $NGT_TIME || test $CURR_TIME -le $MOR_TIME ; then
		PRGM="NGT"
	fi
	
	if test $PRGM_T -eq $PRGM
	then
		return false
	fi

	return true
}

################################################################################
##	This function generates and episode list based in the status of the system.
##	If the remote dir is mounted it  generates a random episode list based
##	in the proclamation specified in the environment variables x_SERIES and in
##	the time of the day.
##	If not mounted, it generates a random episode list using the LOCAL_DIR.
################################################################################
ep_list () {
	FND_DIRS=""


	#This condition checks if remote fs is mounter
	if test ! -d $REMOTE_DIR
	then
		export LIST=($(find $LOCAL_DIR -type f | shuf))
		export REMOTE_PLAY=0
		return 0
	fi
	
	REMOTE_DIR_ALT=$(echo $REMOTE_DIR | sed 's/\//\\\//g')

	##Following conditions checks for morning,afternoon or night time
	CURR_TIME=$(date +%H%M)
	if  test $CURR_TIME -ge $MOR_TIME  &&  test $CURR_TIME -le $LNC_TIME ; then
		export PRGM_T="MOR"
		FND_DIRS=`echo $MOR_SERIES | sed "s/[^ ]*/$REMOTE_DIR_ALT&\//g"`
	elif  test $CURR_TIME -ge $LNC_TIME && test $CURR_TIME -le $NGT_TIME ; then 
		export PRGM_T="LNC"
		FND_DIRS=`echo $LNC_SERIES | sed "s/[^ ]*/$REMOTE_DIR_ALT&\//g"`
	elif  test $CURR_TIME -ge $NGT_TIME || test $CURR_TIME -le $MOR_TIME ; then
		export PRGM_T="NGT"
		FND_DIRS=`echo $NGT_SERIES | sed "s/[^ ]*/$REMOTE_DIR_ALT&\//g"`
	fi
	export LIST=($(find $FND_DIRS -type f,l | shuf ))
	echo $LIST
	export REMOTE_PLAY=1
	
	return 0
}


################################################################################
##	This function close all the processes before stopping the system.
################################################################################
sig_term() {
	umount_remote
	killall omxplayer; killall omxplayer.bin
	exit
}

####################
##	MAIN
####################
while true 
do
	trap sig_term TERM
	mount_remote
	ep_list
	remote_media_watchdog &
	remote_dir_whatchdog &

	for i in "${LIST[@]}"
	do
		echo $i > ~/web/ep.txt
		omxplayer -o hdmi $i
		if [ ! -d $REMOTE_DIR ] && [ $REMOTE_PLAY -eq 1 ]
		then
			break
		elif [ -d $REMOTE_DIR ] && [ $REMOTE_PLAY -eq 0 ]
		then
			break
		fi
	done
	
	
done
